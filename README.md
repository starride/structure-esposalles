# Esposalles - Ground truth for layout analysis

## The Esposalles database
The Esposlles database was used during the ICDAR 2019 Robust Reading competition for Information Extraction in Historical Handwritten Records.
The original annotations contain the bounding box of each word as well as its transcription. 

It can be downloaded [here](https://rrc.cvc.uab.es/?ch=10).

## Annotations
Our task is record detection. We provide annotations to ease future evaluation on this database.
- The bounding box of each record, where each line corresponds to a bounding box using the following format "object_name x1 y1 x2 y2" ;
- Masks of annotated first text-lines ;
- Masks of annotated tax symbols.
